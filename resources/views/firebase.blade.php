<!DOCTYPE html>
<html>
    <head>
        <title>FCM</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <link rel="manifest" href="/manifest.json"> --}}
    </head>
    <body>
        <h4>Firebase Notification</h4>
        <br>
        <a href="{{url("send-notification")}}">Send Notification</a>
    </body>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-messaging.js"></script>

    <script>
        var firebaseConfig = {
            apiKey: "apiKey",
            authDomain: "authDomain",
            projectId: "projectId",
            storageBucket: "storageBucket",
            messagingSenderId: "messagingSenderId",
            appId: "appId",
            measurementId: "measurementId"
        };
        firebase.initializeApp(firebaseConfig);

        // Retrieve Firebase Messaging object.
        const messaging = firebase.messaging();
        messaging.requestPermission()
        .then(function() {
            console.log('Notification permission granted.');
            getRegToken()
            if(isTokenSentToServer()) {
                console.log('Token already saved.');
            } else {
                getRegToken();
            }
        })
        .catch(function(err) {
            console.log('Unable to get permission to notify.', err);
        });

        function getRegToken(argument) {
            messaging.getToken().then(function(currentToken) {
                if (currentToken) {
                    console.log(currentToken);
                    saveToken(currentToken);
                    setTokenSentToServer(true);
                } else {
                    console.log('No Instance ID token available. Request permission to generate one.');
                    setTokenSentToServer(false);
                }
            })
            .catch(function(err) {
                console.log('An error occurred while retrieving token. ', err);
                setTokenSentToServer(false);
            });
        }

        function setTokenSentToServer(sent) {
            window.localStorage.setItem('sentToServer', sent ? 1 : 0);
        }

        function isTokenSentToServer() {
            return window.localStorage.getItem('sentToServer') == 1;
        }

        function saveToken(currentToken) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ route('save-token') }}',
                method: 'post',
                data: 'token=' + currentToken
            }).done(function(result){
                console.log(result);
            })
        }

        messaging.onMessage(function(payload) {
            console.log("Message received. ", payload);
            notificationTitle = payload.notification.title;
            notificationOptions = {
                body: payload.notification.body,
                icon: payload.notification.icon,
                image:  payload.notification.image
            };
            var notification = new Notification(notificationTitle,notificationOptions);
        });
    </script>
</html>
