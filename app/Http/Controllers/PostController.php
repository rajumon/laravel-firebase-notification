<?php

namespace App\Http\Controllers;

use App\Jobs\SendPushNotification;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    function index(){
        $token = Post::all();
        return view('firebase', compact('token'));
    }

    function sendNotification(){
        $token = 'Brouser Token';
        $from = "Server key";
        $msg = array
        (
            'body'  => "Demo 2",
            'title' => "Hi,Raju",
            'receiver' => 'erw',
            'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
            'sound' => 'mySound'/*Default sound*/
        );

        $fields = array
        (
            'to'        => $token,
            'notification'  => $msg
        );

        $headers = array
        (
            'Authorization: key=' . $from,
            'Content-Type: application/json'
        );
        //#Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        dd($result);
        curl_close( $ch );
    }

    function saveToken(Request $request){
        $token = Post::updateOrCreate(array('token' => $request->input('token')));
        $token->token = $request->input('token');
        $token->save();
    }
}
